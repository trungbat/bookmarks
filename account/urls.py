from django.urls import path
from django.contrib.auth import views as auth_views 
from django.conf import settings
from django.conf.urls.static import static
from . import views 


urlpatterns = [
	path('', views.HomePageView, name='home'),
	path('register/', views.register, name = 'register'),
	path('edit/', views.edit, name = 'edit'),
]

if settings.DEBUG:
	urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
