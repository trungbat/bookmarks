from django.contrib.auth.models import User
from django import forms
from .models import Profile

class UserRegistrationForm(forms.ModelForm):
	password = forms.CharField(label ='Mật khẩu',
								widget = forms.PasswordInput)
	password2 = forms.CharField(label ='Nhập lại mật khẩu',
								widget = forms.PasswordInput)

	class Meta:
		model = User 
		fields = ('username', 'first_name','email')

	def clean_password2(self):
		cd = self.cleaned_data
		if cd['password'] != cd["password2"]:
			raise forms.ValidationError('Password don\'t match')

		return cd['password']


class UserEditForm(forms.ModelForm):
	class Meta:
		model = User 
		fields = ('first_name','last_name','email')
		labels= {
			'first_name':'Họ',
			'last_name':'Tên',
		}
MONTHS = {
    1:('Tháng 1'), 2:('Tháng 2'), 3:('Tháng 3'), 4:('Tháng 4'),
    5:('Tháng 5'), 6:('Tháng 6'), 7:('Tháng 7'), 8:('Tháng 8'),
    9:('Tháng 9'), 10:('Tháng 10'), 11:('Tháng 11'), 12:('Tháng 12')
}
class ProfileEditForm(forms.ModelForm):
	class Meta:
		model = Profile
		fields = ('date_of_birth','photo')
		widgets = {
			'date_of_birth': forms.SelectDateWidget(empty_label=("Năm", "Tháng", "Ngày"),months= MONTHS, years=range(2018,1900,-1)),
		}


